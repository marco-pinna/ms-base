process.env.SUPPRESS_NO_CONFIG_WARNING = 'y';
const configInit = require("config");
const Path = require('path');

module.exports = () => {
    const etcConfigs = require('./requireAll')({
        dirname     :  Path.join( process.cwd(), 'src'),
        filter      :  function(name, path){
            return path.match(/etc\/config\.js$/);
        }
    });


    etcConfigs.map(function(etcConfig) {
        Object.entries(etcConfig).forEach(function([moduleName, moduleConfig]) {
            let util = configInit.util

            configInit[moduleName] = configInit[moduleName] || {};
            util.extendDeep(configInit[moduleName], moduleConfig);
            util.attachProtoDeep(configInit[moduleName])
        })
    })

    let CONFIG_DIR = configInit.util.initParam('NODE_CONFIG_DIR', Path.join( process.cwd(), 'config') );
    if (CONFIG_DIR.indexOf('.') === 0) {
        CONFIG_DIR = Path.join(process.cwd() , CONFIG_DIR);
    }

    let fullFilename = Path.join(CONFIG_DIR ,  'env.json' );
    let configObj = configInit.util.parseFile(fullFilename);
    if (configObj) {
        configInit.util.extendDeep(configInit, configObj);
    }
}
