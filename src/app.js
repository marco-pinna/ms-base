let server = null;

module.exports = {
    async init (){
        //init configurazioni
        let configInit = require("./configInit");
        configInit();

        let serverInit = require("./serverInit");
        server = await serverInit()

        let cronInit = require("./cronInit")
        cronInit()
    },

    getServer (){
        return server;
    }
}