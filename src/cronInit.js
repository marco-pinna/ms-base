const config = require("config");
const CronJob = require('cron').CronJob;
const Path = require('path');

module.exports = function() {
    let crons = []
    try {
        crons = config.get("crons")
    }catch(e){
        return;
    }

    let base_path = Path.join( process.cwd(), 'src')

    for (const [name, cronInfo] of Object.entries(crons)) {
        let functionToRun = require(base_path+"/"+cronInfo.run_function);

        let job = new CronJob(cronInfo.cron_expr,functionToRun, null, true );

        console.log(`CRON ${name}: ${cronInfo.cron_expr} function: ${cronInfo.run_function}`);
    }

}