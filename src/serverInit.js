const Hapi = require('hapi');
const config = require("config");
const Path = require('path');

const etcRoutes = require('./requireAll')({
    dirname     :  Path.join( process.cwd(), 'src'),
    filter      :  function(name, path){
        return path.match(/etc\/routes\.js$/);
    }
});

module.exports = async function() {
    let serverConfig = null;
    try{
        serverConfig = config.get("core.server");
    }catch(e){
        return false;
    }


    const server = Hapi.server({
        port: serverConfig.port,
        host: serverConfig.host
    });

    await server.register([
            require("inert"),
            require("vision"),
            {
                plugin: require("hapi-swagger"),
                options: {
                    info: {
                        title: "ms-inventory API",
                        version: "1.0"
                    }
                }
            }
        ]
    );


    let routes = [];
    etcRoutes.map(function(route) {
        routes = [...routes,...route];
    });

    if (routes.length > 0) {
        server.route(routes);
        await server.start();
        console.log(`Server running at: ${server.info.uri}`);
        return server
    }

    return false;
};
